﻿# Tags:
# rank_headgear: any headgear that signifies a certain title. crowns, pope hat etc.



headgear = {
	# interface_position = 1
	usage = game
	selection_behavior = max
	
	no_hat = {
		usage = game 
		dna_modifiers = {
			accessory = {
				mode = add
		
				gene = headgear
				template = no_headgear
				value = 0
			}
		}
		weight = {
			base = 5
			modifier = {
				add = 100
				AND = {
					should_be_naked_trigger = yes
				}
			}
		}
	}

## So that the Prophet Muhammad does not have a portrait ## 

	no_portrait = {
		dna_modifiers = {
			accessory = {
				mode = add				
				gene = headgear
				template = no_headgear
				value = 0.5
			}
		}
		weight = {
			base = 0
		}
	}


## No headgear for children under 16 ## 

	children_no_headgear = {

		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = no_headgear
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 100
				age < 16
				portrait_egyptian_clothing_trigger = no # Egyptian Child Pharaohs can wear their Nemes
			}
		}
	}
# Egyptian
	egyptian_pharaoh = {

		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = egyptian_headgear_high
				range = { 0 1 } # For the randomness to work correctly
			}
			accessory = {
				mode = add
				gene = headgear_2
				template = egyptian_pharaoh
				range = { 0 1 } # For the randomness to work correctly
			}
			accessory = {
				mode = add
				gene = headgear_3
				template = egyptian_pharaoh
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 1000
				AND = {
					OR = {
						highest_held_title_tier = tier_kingdom
						highest_held_title_tier = tier_empire
					}
					portrait_egyptian_clothing_trigger = yes
				}

			}
		}
	}
	
	egyptian_royalty = {

		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = egyptian_headgear_high
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 750
				AND = {
					highest_held_title_tier = tier_duchy
					portrait_egyptian_clothing_trigger = yes
				}

			}
		}
	}
	
	egyptian_noble = {

		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = egyptian_headgear_low
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 500
				AND = {
					OR = {
						highest_held_title_tier = tier_county
						highest_held_title_tier = tier_barony
						AND = {
							is_councillor = yes
							trigger_if = {
								limit = { holds_landed_title = yes }
								NOT = { highest_held_title_tier >= tier_duchy }
							}
						}
					}
					portrait_egyptian_clothing_trigger = yes
				}
			}
		}
	}	
	
#Mycenaean
	aegean_war = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = aegean_war
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 100
				portrait_wear_helmet_trigger = yes
				portrait_aegean_clothing_trigger = yes
			}
		}
	}
	mycenaean_war = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = mycenaean_war
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 100
				portrait_wear_helmet_trigger = yes
				portrait_mycenaean_clothing_trigger = yes
			}
		}
	}
	mycenaean_royalty = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = mycenaean_royalty
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 50
				OR = {
					AND = {
						OR = {
							highest_held_title_tier = tier_duchy
							highest_held_title_tier = tier_kingdom
							highest_held_title_tier = tier_empire
							has_government = theocracy_government
						}
						NOT = { has_government = mercenary_government } # Blocked for mercenaries
						portrait_mycenaean_clothing_trigger = yes
					}
					AND = {
						exists = primary_spouse
						is_ruler = no
						primary_spouse = {
							OR = {
								highest_held_title_tier = tier_duchy
								highest_held_title_tier = tier_kingdom
								highest_held_title_tier = tier_empire
								has_government = theocracy_government
							}
							portrait_mycenaean_clothing_spouse_trigger = yes
						}
					}
				}
			}
		}
	}

## Western ##

	western_high_nobles = { # Standard Crown
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = western_high_nobility
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 50
				OR = {
					AND = {
						highest_held_title_tier >= tier_duchy
						NOT = { has_government = mercenary_government } # Blocked for mercenaries
						portrait_standard_crown_trigger = yes
					}
					AND = {
						exists = primary_spouse
						is_ruler = no
						primary_spouse = {
							highest_held_title_tier >= tier_duchy
							portrait_standard_crown_spouse_trigger = yes
						}
					}
				}
			}
		}
	}

	western_war = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = western_war
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 100
				portrait_wear_helmet_trigger = yes
				portrait_generic_western_trigger = yes
			}
		}
	}

## INDIAN ##

	indian_war = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = indian_war
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 100
				portrait_wear_helmet_trigger = yes
				portrait_indian_clothing_trigger = yes
			}
		}
	}


## MENA ##
	mena_war = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = mena_war
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 100
				portrait_wear_helmet_trigger = yes
				portrait_generic_eastern_trigger = yes
			}
		}
	}

## Sub-Saharan Africans ##

	african_commoners = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = sub_saharan_common
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 50
				highest_held_title_tier < tier_county
				NOT = { has_government = theocracy_government }
				portrait_african_clothing_trigger = yes
			}
		}
	}

	sub_saharan_high_nobles = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = sub_saharan_high_nobility
				range = { 0 1 } # For the randomness to work correctly
			}
		}		
		weight = {
			base = 0
			modifier = {
				add = 50
				OR = {
					AND = {
						OR = {
							highest_held_title_tier = tier_duchy
							has_government = theocracy_government
						}
						NOT = { has_government = mercenary_government } # Blocked for mercenaries
						portrait_african_clothing_trigger = yes
					}
					AND = {
						exists = primary_spouse
						is_ruler = no
						primary_spouse = {
							OR = {
								highest_held_title_tier = tier_duchy
								has_government = theocracy_government
							}
							portrait_african_clothing_spouse_trigger = yes
						}
					}
				}

			}
		}
	}

	sub_saharan_royalty = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = sub_saharan_royalty
				range = { 0 1 } # For the randomness to work correctly
			}
		}		
		weight = {
			base = 0
			modifier = {
				add = 50
				OR = {
					AND = {
						OR = {
							highest_held_title_tier = tier_kingdom
							has_government = theocracy_government
						}
						NOT = { has_government = mercenary_government } # Blocked for mercenaries
						portrait_african_clothing_trigger = yes
					}
					AND = {
						exists = primary_spouse
						is_ruler = no
						primary_spouse = {
							OR = {
								highest_held_title_tier = tier_kingdom
								has_government = theocracy_government
							}
							NOT = { has_government = mercenary_government } # Blocked for mercenaries
							portrait_african_clothing_spouse_trigger = yes
						}
					}
				}

			}
		}
	}

	sub_saharan_imperial = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = sub_saharan_imperial
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 50
				highest_held_title_tier = tier_empire
				NOR = {
					has_government = mercenary_government
					has_government = theocracy_government
				}
				portrait_african_clothing_trigger = yes
			}
		}
	}


	sub_saharan_war = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = headgear
				template = mena_war
				range = { 0 1 } # For the randomness to work correctly
			}
		}	
		weight = {
			base = 0
			modifier = {
				add = 100
				portrait_wear_helmet_trigger = yes
				portrait_african_clothing_trigger = yes
			}
		}
	}
}