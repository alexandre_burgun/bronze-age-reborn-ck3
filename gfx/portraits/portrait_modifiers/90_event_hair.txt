﻿
event_hair = {
	usage = game
	priority = 10

	no_hair = {
		dna_modifiers = {
			accessory = {
				mode = add
				gene = hairstyles
				template = no_hairstyles
				value = 0
			}
		}   
		outfit_tags = { bald }
		weight = {
			base = 0
		}
	}
}
