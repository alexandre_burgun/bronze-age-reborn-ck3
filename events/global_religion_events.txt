﻿namespace = global_religion


#When a pagan (ai) domain is ready to feudalize, a reformed neighbor they're on good terms with should attempt to convert them, or they should have a chance to spontaneously convert


#########################################################################
# Pagan reformed by neighbor or spontaneously 							#
# by Linnéa Thimrén														#
# global_religion.0001-0009												#
#########################################################################


global_religion.0001 = {
	hidden = yes
	
	immediate = {
	}
}