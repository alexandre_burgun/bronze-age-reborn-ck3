﻿heavy_infantry_max_size_add = {
	decimals = 0
}

heavy_infantry_max_size_mult = {
	decimals = 0
	percent = yes
}

heavy_infantry_siege_value_add = {
	decimals = 1
}

heavy_infantry_siege_value_mult = {
	decimals = 0
	percent = yes
}

heavy_infantry_damage_add = {
	decimals = 0
}

heavy_infantry_damage_mult = {
	decimals = 0
	percent = yes
}

heavy_infantry_toughness_add = {
	decimals = 0
}

heavy_infantry_toughness_mult = {
	decimals = 0
	percent = yes
}

heavy_infantry_pursuit_add = {
	decimals = 0
}

heavy_infantry_pursuit_mult = {
	decimals = 0
	percent = yes
}

heavy_infantry_screen_add = {
	decimals = 0
}

heavy_infantry_screen_mult = {
	decimals = 0
	percent = yes
}

heavy_infantry_maintenance_mult = {
	decimals = 0
	color = bad
	percent = yes
}

heavy_infantry_recruitment_cost_mult = {
	decimals = 0
	color = bad
	percent = yes
}

spearmen_max_size_add = {
	decimals = 0
}

spearmen_max_size_mult = {
	decimals = 0
	percent = yes
}

spearmen_siege_value_add = {
	decimals = 1
}

spearmen_siege_value_mult = {
	decimals = 0
	percent = yes
}

spearmen_damage_add = {
	decimals = 0
}

spearmen_damage_mult = {
	decimals = 0
	percent = yes
}

spearmen_toughness_add = {
	decimals = 0
}

spearmen_toughness_mult = {
	decimals = 0
	percent = yes
}

spearmen_pursuit_add = {
	decimals = 0
}

spearmen_pursuit_mult = {
	decimals = 0
	percent = yes
}

spearmen_screen_add = {
	decimals = 0
}

spearmen_screen_mult = {
	decimals = 0
	percent = yes
}

spearmen_maintenance_mult = {
	decimals = 0
	color = bad
	percent = yes
}

spearmen_recruitment_cost_mult = {
	decimals = 0
	color = bad
	percent = yes
}

archers_max_size_add = {
	decimals = 0
}

archers_max_size_mult = {
	decimals = 0
	percent = yes
}

archers_siege_value_add = {
	decimals = 1
}

archers_siege_value_mult = {
	decimals = 0
	percent = yes
}

archers_damage_add = {
	decimals = 0
}

archers_damage_mult = {
	decimals = 0
	percent = yes
}

archers_toughness_add = {
	decimals = 0
}

archers_toughness_mult = {
	decimals = 0
	percent = yes
}

archers_pursuit_add = {
	decimals = 0
}

archers_pursuit_mult = {
	decimals = 0
	percent = yes
}

archers_screen_add = {
	decimals = 0
}

archers_screen_mult = {
	decimals = 0
	percent = yes
}

archers_maintenance_mult = {
	decimals = 0
	color = bad
	percent = yes
}

archers_recruitment_cost_mult = {
	decimals = 0
	color = bad
	percent = yes
}

light_cavalry_max_size_add = {
	decimals = 0
}

light_cavalry_max_size_mult = {
	decimals = 0
	percent = yes
}

light_cavalry_siege_value_add = {
	decimals = 1
}

light_cavalry_siege_value_mult = {
	decimals = 0
	percent = yes
}

light_cavalry_damage_add = {
	decimals = 0
}

light_cavalry_damage_mult = {
	decimals = 0
	percent = yes
}

light_cavalry_toughness_add = {
	decimals = 0
}

light_cavalry_toughness_mult = {
	decimals = 0
	percent = yes
}

light_cavalry_pursuit_add = {
	decimals = 0
}

light_cavalry_pursuit_mult = {
	decimals = 0
	percent = yes
}

light_cavalry_screen_add = {
	decimals = 0
}

light_cavalry_screen_mult = {
	decimals = 0
	percent = yes
}

light_cavalry_maintenance_mult = {
	decimals = 0
	color = bad
	percent = yes
}

light_cavalry_recruitment_cost_mult = {
	decimals = 0
	color = bad
	percent = yes
}

heavy_chariot_max_size_add = {
	decimals = 0
}

heavy_chariot_max_size_mult = {
	decimals = 0
	percent = yes
}

heavy_chariot_siege_value_add = {
	decimals = 1
}

heavy_chariot_siege_value_mult = {
	decimals = 0
	percent = yes
}

heavy_chariot_damage_add = {
	decimals = 0
}

heavy_chariot_damage_mult = {
	decimals = 0
	percent = yes
}

heavy_chariot_toughness_add = {
	decimals = 0
}

heavy_chariot_toughness_mult = {
	decimals = 0
	percent = yes
}

heavy_chariot_pursuit_add = {
	decimals = 0
}

heavy_chariot_pursuit_mult = {
	decimals = 0
	percent = yes
}

heavy_chariot_screen_add = {
	decimals = 0
}

heavy_chariot_screen_mult = {
	decimals = 0
	percent = yes
}

heavy_chariot_maintenance_mult = {
	decimals = 0
	color = bad
	percent = yes
}

heavy_chariot_recruitment_cost_mult = {
	decimals = 0
	color = bad
	percent = yes
}

skirmishers_max_size_add = {
	decimals = 0
}

skirmishers_max_size_mult = {
	decimals = 0
	percent = yes
}

skirmishers_siege_value_add = {
	decimals = 1
}

skirmishers_siege_value_mult = {
	decimals = 0
	percent = yes
}

skirmishers_damage_add = {
	decimals = 0
}

skirmishers_damage_mult = {
	decimals = 0
	percent = yes
}

skirmishers_toughness_add = {
	decimals = 0
}

skirmishers_toughness_mult = {
	decimals = 0
	percent = yes
}

skirmishers_pursuit_add = {
	decimals = 0
}

skirmishers_pursuit_mult = {
	decimals = 0
	percent = yes
}

skirmishers_screen_add = {
	decimals = 0
}

skirmishers_screen_mult = {
	decimals = 0
	percent = yes
}

skirmishers_maintenance_mult = {
	decimals = 0
	color = bad
	percent = yes
}

skirmishers_recruitment_cost_mult = {
	decimals = 0
	color = bad
	percent = yes
}

light_chariot_max_size_add = {
	decimals = 0
}

light_chariot_max_size_mult = {
	decimals = 0
	percent = yes
}

light_chariot_siege_value_add = {
	decimals = 1
}

light_chariot_siege_value_mult = {
	decimals = 0
	percent = yes
}

light_chariot_damage_add = {
	decimals = 0
}

light_chariot_damage_mult = {
	decimals = 0
	percent = yes
}

light_chariot_toughness_add = {
	decimals = 0
}

light_chariot_toughness_mult = {
	decimals = 0
	percent = yes
}

light_chariot_pursuit_add = {
	decimals = 0
}

light_chariot_pursuit_mult = {
	decimals = 0
	percent = yes
}

light_chariot_screen_add = {
	decimals = 0
}

light_chariot_screen_mult = {
	decimals = 0
	percent = yes
}

light_chariot_maintenance_mult = {
	decimals = 0
	color = bad
	percent = yes
}

light_chariot_recruitment_cost_mult = {
	decimals = 0
	color = bad
	percent = yes
}

siege_weapon_max_size_add = {
	decimals = 0
}

siege_weapon_max_size_mult = {
	decimals = 0
	percent = yes
}

siege_weapon_siege_value_add = {
	decimals = 1
}

siege_weapon_siege_value_mult = {
	decimals = 0
	percent = yes
}

siege_weapon_damage_add = {
	decimals = 0
}

siege_weapon_damage_mult = {
	decimals = 0
	percent = yes
}

siege_weapon_toughness_add = {
	decimals = 0
}

siege_weapon_toughness_mult = {
	decimals = 0
	percent = yes
}

siege_weapon_pursuit_add = {
	decimals = 0
}

siege_weapon_pursuit_mult = {
	decimals = 0
	percent = yes
}

siege_weapon_screen_add = {
	decimals = 0
}

siege_weapon_screen_mult = {
	decimals = 0
	percent = yes
}

siege_weapon_maintenance_mult = {
	decimals = 0
	color = bad
	percent = yes
}

siege_weapon_recruitment_cost_mult = {
	decimals = 0
	color = bad
	percent = yes
}
