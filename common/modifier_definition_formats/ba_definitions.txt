﻿mountain_valley_attrition_mult = {
	decimals = 0
	color = bad
	percent = yes
}

mountain_valley_cancel_negative_supply = {
	decimals = 0
}

mountain_valley_advantage = {
	decimals = 0
}

mountain_valley_min_combat_roll = {
	decimals = 0
}

mountain_valley_max_combat_roll = {
	decimals = 0
}

mountain_valley_development_growth = {
	decimals = 1
}

mountain_valley_development_growth_factor = {
	decimals = 0
	percent = yes
}

mountain_valley_construction_gold_cost = {
	decimals = 0
	color = bad
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

mountain_valley_holding_construction_gold_cost = {
	decimals = 0
	color = bad
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

mountain_valley_construction_piety_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PIETY_PREFIX
	percent = yes
}

mountain_valley_holding_construction_piety_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PIETY_PREFIX
	percent = yes
}

mountain_valley_construction_prestige_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PRESTIGE_PREFIX
	percent = yes
}

mountain_valley_holding_construction_prestige_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PRESTIGE_PREFIX
	percent = yes
}

mountain_valley_supply_limit = {
	decimals = 0
}

mountain_valley_supply_limit_mult = {
	decimals = 0
	percent = yes
}

mountain_valley_tax_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

mountain_valley_levy_size = {
	decimals = 0
	prefix = MOD_SOLDIER_PREFIX
	percent = yes
}
desert_hills_attrition_mult = {
	decimals = 0
	color = bad
	percent = yes
}

desert_hills_cancel_negative_supply = {
	decimals = 0
}

desert_hills_advantage = {
	decimals = 0
}

desert_hills_min_combat_roll = {
	decimals = 0
}

desert_hills_max_combat_roll = {
	decimals = 0
}

desert_hills_development_growth = {
	decimals = 1
}

desert_hills_development_growth_factor = {
	decimals = 0
	percent = yes
}

desert_hills_construction_gold_cost = {
	decimals = 0
	color = bad
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

desert_hills_holding_construction_gold_cost = {
	decimals = 0
	color = bad
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

desert_hills_construction_piety_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PIETY_PREFIX
	percent = yes
}

desert_hills_holding_construction_piety_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PIETY_PREFIX
	percent = yes
}

desert_hills_construction_prestige_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PRESTIGE_PREFIX
	percent = yes
}

desert_hills_holding_construction_prestige_cost = {
	decimals = 0
	color = bad
	prefix = MOD_PRESTIGE_PREFIX
	percent = yes
}

desert_hills_supply_limit = {
	decimals = 0
}

desert_hills_supply_limit_mult = {
	decimals = 0
	percent = yes
}

desert_hills_tax_mult = {
	decimals = 0
	prefix = MOD_GOLD_PREFIX
	percent = yes
}

desert_hills_levy_size = {
	decimals = 0
	prefix = MOD_SOLDIER_PREFIX
	percent = yes
}
