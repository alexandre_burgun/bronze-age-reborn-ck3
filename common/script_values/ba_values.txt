﻿test_patronage_svalue = {
	value = 0
	every_in_global_list = {
		variable = unused_patronages
		add = 1
	}
}
development_decay_level = {
	value = development_level
	if = { limit = { holder.culture = { has_innovation = innovation_city_planning } }
		add = -10
	}
	if = { limit = { holder.culture = { has_innovation = innovation_development_2 } }
		add = -10
	}
	if = { limit = { holder.culture = { has_innovation = innovation_development_3 } }
		add = -10
	}
	if = { limit = { holder.culture = { has_innovation = innovation_development_4 } }
		add = -10
	}
	min = 0
	max = 100
}
bce_date_svalue = 2900
didnt_kill_deformed_piety_loss = -250
killed_deformed_piety_gain = 100
killed_nondeformed_piety_gain = 25
days_to_add_tomb_level = 1095 # 3 years, add more once different types with doctrines are in