﻿#Education focuses
#List them in the order they appear in the interface, clockwise

prowess_adventurer_focus = {
	lifestyle = prowess_lifestyle

	desc = {
		desc = prowess_adventurer_focus_desc
		desc = line_break
	}
	
	modifier = {
		prowess = 1
		knight_limit = 2
	}

	auto_selection_weight = {
		value = 11
		if = {
			limit = {
				has_education_martial_trigger = yes
			}
			add = 1989
		}
	}

	focus_id = 15
}

prowess_duelist_focus = {
	lifestyle = prowess_lifestyle

	desc = {
		desc = prowess_duelist_focus_desc
		desc = line_break
	}
	
	modifier = {
		prowess = 3
	}

	auto_selection_weight = {
		value = 11
		if = {
			limit = {
				has_education_martial_trigger = yes
			}
			add = 1989
		}
		if = {
			limit = {
				has_trait = arrogant
			}
			multiply = 2
		}
		if = {
			limit = {
				has_trait = shy
			}
			multiply = 0
		}
	}

	focus_id = 16
}

prowess_guardian_focus = {
	lifestyle = prowess_lifestyle

	desc = {
		desc = prowess_guardian_focus_desc
		desc = line_break
	}
	
	modifier = {
		prowess = 1
		health = 0.25
	}

	auto_selection_weight = {
		value = 11
		if = {
			limit = {
				has_education_martial_trigger = yes
			}
			add = 1989
		}
		if = {
			limit = {
				has_trait = brave
			}
			multiply = 5
		}
		if = {
			limit = {
				has_trait = honest
			}
			multiply = 2
		}
		if = {
			limit = {
				has_trait = chaste
			}
			multiply = 1.5
		}
	}

	focus_id = 17
}
