﻿should_use_tribal_realm_palace_graphics_trigger = {
	#has_government = tribal_government
	always = no
}

has_graphical_mena_culture_group_trigger = {
	has_building_gfx = mena_building_gfx
}

has_graphical_india_culture_group_trigger = {
	has_building_gfx = indian_building_gfx
}

has_graphical_mediterranean_culture_group_trigger = {
	has_building_gfx = mediterranean_building_gfx
}

has_graphical_western_culture_group_trigger = {
	has_building_gfx = western_building_gfx
}

has_graphical_norse_culture_group_trigger = {
	always = no
}

has_graphical_african_culture_group_trigger = {
	always = no
}

has_graphical_steppe_culture_group_trigger = {
	always = no
}

has_graphical_mycenaean_region_trigger = {
	trigger_if = {
		limit = {
			exists = capital_province
		}
		capital_province = {
			geographical_region = graphical_mycenaean
		}
	}
	trigger_if = {
		limit = {
			exists = host.capital_province
			NOT = { exists = capital_province }
		}
		host.capital_province = {
			geographical_region = graphical_mycenaean
		}
	}
	trigger_if = {
		limit = {
			exists = liege.capital_province
			NOT = { exists = capital_province }
		}
		liege.capital_province = {
			geographical_region = graphical_mycenaean
		}
	}
	trigger_if = {
		limit = {
			exists = location.province_owner.capital_province
			NOT = { exists = capital_province }
		}
		location.province_owner.capital_province = {
			geographical_region = graphical_mycenaean
		}
	}
}

graphical_wilderness_desert_trigger = {
	OR = {
		terrain = floodplains
		terrain = desert
		terrain = desert_mountains
		terrain = oasis
		terrain = desert_hills
		AND = {
			terrain = hills
			NOR = {
				geographical_region = world_greekaegean
				geographical_region = world_anatolia
			}
		}
	}
}

graphical_wilderness_forest_pine_trigger = {
	OR = {
		terrain = taiga
		terrain = forest
	}
	always = no
}

graphical_wilderness_forest_trigger = {
	OR = {
		terrain = jungle
		terrain = forest
		terrain = farmlands
		terrain = wetlands
		terrain = plains
		AND = {
			terrain = hills
			OR = {
				geographical_region = world_greekaegean
				geographical_region = world_anatolia
			}
		}
	}
}

graphical_wilderness_mountains_trigger = {
	terrain = mountains
	terrain = mountain_valley
}

graphical_wilderness_steppe_trigger = {
	OR = {
		terrain = steppe
		terrain = drylands
	}
}

### Scripted illustrations

# This doesn't really evaluate anything, it just saves the scope out for later things to use
title_illustration_save_province_temporary = {
	trigger_if = {
		limit = { tier = tier_barony }
		title_province = {
			save_temporary_scope_as = province
		}
	}
	trigger_else_if = {
		limit = {
			tier > tier_county
			NOT = {
				any_in_de_jure_hierarchy = { tier = tier_county }
			}
		}
	}
	trigger_else = {
		title_capital_county.title_province = {
			save_temporary_scope_as = province
		}
	}
	always = yes
}

#has_indian_culture_trigger = {
has_graphical_india_region_trigger = {
	trigger_if = {
		limit = {
			exists = capital_province
		}
		capital_province = {
			geographical_region = graphical_india
		}
	}
	trigger_else_if = {
		limit = {
			NOT = { exists = capital_province }
		}
		trigger_if = { 
			limit = { exists = host.capital_province }
			host.capital_province = {
				geographical_region = graphical_india
			}
		}
		trigger_else_if = {
			limit = {
				exists = liege.capital_province
			}
			liege.capital_province = {
				geographical_region = graphical_india
			}
		}
		trigger_else_if = {
			limit = {
				exists = location.province_owner.capital_province
			}
			location.province_owner.capital_province = {
				geographical_region = graphical_india
			}
		}
		trigger_else = { always = yes }
	}
	trigger_else = { always = yes }
}

#has_mediterranean_culture_trigger = {
has_graphical_mediterranean_region_trigger = {
	trigger_if = {
		limit = {
			exists = capital_province
		}
		capital_province = {
			geographical_region = graphical_mediterranean
		}
	}
	trigger_else_if = {
		limit = {
			NOT = { exists = capital_province }
		}
		trigger_if = { 
			limit = { exists = host.capital_province }
			host.capital_province = {
				geographical_region = graphical_mediterranean
			}
		}
		trigger_else_if = {
			limit = {
				exists = liege.capital_province
			}
			liege.capital_province = {
				geographical_region = graphical_mediterranean
			}
		}
		trigger_else_if = {
			limit = {
				exists = location.province_owner.capital_province
			}
			location.province_owner.capital_province = {
				geographical_region = graphical_mediterranean
			}
		}
		trigger_else = { always = yes }
	}
	trigger_else = { always = yes }
}

#has_mena_culture_trigger = {
has_graphical_mena_region_trigger = {
	trigger_if = {
		limit = {
			exists = capital_province
		}
		capital_province = {
			geographical_region = graphical_mena
		}
	}
	trigger_else_if = {
		limit = {
			NOT = { exists = capital_province }
		}
		trigger_if = { 
			limit = { exists = host.capital_province }
			host.capital_province = {
				geographical_region = graphical_mena
			}
		}
		trigger_else_if = {
			limit = {
				exists = liege.capital_province
			}
			liege.capital_province = {
				geographical_region = graphical_mena
			}
		}
		trigger_else_if = {
			limit = {
				exists = location.province_owner.capital_province
			}
			location.province_owner.capital_province = {
				geographical_region = graphical_mena
			}
		}
		trigger_else = { always = yes }
	}
	trigger_else = { always = yes }
}

has_graphical_scandinavia_region_trigger = {
	always = no
}

#has_western_culture_trigger = {
has_graphical_western_region_trigger = {
	always = no
}
