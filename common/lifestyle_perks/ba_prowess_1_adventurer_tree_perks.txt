﻿opportunistic_friends_perk = {
	lifestyle = prowess_lifestyle
	tree = adventurer
	position = { 2 0 }
	icon = node_martial
	
	auto_selection_weight = {
		value = 11
		if = {
			limit = {
				has_education_martial_trigger = yes
			}
			add = 1989
		}
		if = {
			limit = {
				has_focus = prowess_adventurer_focus
			}
			multiply = 5
		}
		if = {
			limit = {
				can_start_new_lifestyle_tree_trigger = no
			}
			multiply = 0
		}
	}
	character_modifier = {
		mercenary_hire_cost_mult = -0.2
	}
}

eye_on_the_horizon_perk = {
	lifestyle = prowess_lifestyle
	tree = adventurer
	position = { 0 1.25 }
	icon = node_martial
	
	parent = opportunistic_friends_perk
	
	character_modifier = {
		title_creation_cost_mult = -0.33
	}
}


ardent_followers_perk = {
	lifestyle = prowess_lifestyle
	tree = adventurer
	position = { 2 1.25 }
	icon = node_martial

	parent = opportunistic_friends_perk

	character_modifier = {
		knight_limit = 4
	}
}

the_journey_is_the_reward_perk = {
	lifestyle = prowess_lifestyle
	tree = adventurer
	position = { 4 1.25 }
	icon = node_martial
	
	parent = opportunistic_friends_perk
	
	character_modifier = {
		diplomatic_range_mult = 0.5
	}

	effect = {
		custom_description_no_bullet = {
			text = quests_perk_effect
		}
	}
}


forceful_suitor_perk = {
	lifestyle = prowess_lifestyle
	tree = adventurer
	position = { 0 2.5 }
	icon = node_martial
	
	parent = eye_on_the_horizon_perk
	
	effect = {
		custom_description_no_bullet = {
			text = abduct_and_marry_effect
		}
	}
}


practice_makes_perfect_perk = {
	lifestyle = prowess_lifestyle
	tree = adventurer
	position = { 2 2.5 }
	icon = node_martial

	parent = ardent_followers_perk

	character_modifier = {
		knight_effectiveness_mult = 0.50
	}
}

inspirational_spirit_perk = {
	lifestyle = prowess_lifestyle
	tree = adventurer
	position = { 4 2.5 }
	icon = node_martial
	
	parent = the_journey_is_the_reward_perk
	
	character_modifier = {
		county_opinion_add = 25
		general_opinion = 5
	}
}


companions_perk = {
	lifestyle = prowess_lifestyle
	tree = adventurer
	position = { 2 3.75 }
	icon = node_martial
	
	parent = forceful_suitor_perk
	parent = practice_makes_perfect_perk
	parent = inspirational_spirit_perk
	
	effect = {
		custom_description_no_bullet = {
			text = companions_perk_effect
		}
	}
}


adventurer_perk = {
	lifestyle = prowess_lifestyle
	tree = adventurer
	position = { 2 5 }
	icon = trait_adventurer #Add new trait
	
	parent = companions_perk
	
	effect = {
		add_trait_force_tooltip = ba_adventurer
	}
}
