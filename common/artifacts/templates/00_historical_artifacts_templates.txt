﻿branch_general_template = {
	can_equip = {
		always = yes
	}

	# can this character benefit from the full modifiers of the artifact?
	can_benefit = {
		scope:artifact = { exists = var:relic_religion }
		has_religion = scope:artifact.var:relic_religion
	}

	# if a given character does not pass the "can_benefit" trigger then this modifier will be applied instead.
	fallback = {
		court_grandeur_baseline_add = 3
	}

	unique = yes
}


### Court Artifacts Templates ###

religious_statue_template = {
	can_equip = {
		always = yes
	}

	can_benefit = {
		scope:artifact = { has_variable = statue_religion }
		religion = scope:artifact.var:statue_religion
	}

	fallback = {
		court_grandeur_baseline_add = 3
	}
}

6050_relic_template = {
	can_equip = {
		scope:artifact = { has_variable = statue_religion }
		religion = scope:artifact.var:statue_religion
	}

	can_benefit = {
		scope:artifact = { has_variable = statue_religion }
		religion = scope:artifact.var:statue_religion
	}

	fallback = {
		monthly_prestige = 0.2
	}
}

general_unique_template = {
	can_equip = {
		always = yes
	}

	can_benefit = {
	}

	fallback = {

	}

	unique = yes
}

child_toy_template = {
	can_equip = {	
		trigger_if = {
			limit = {
				NOT = {
					scope:artifact = { category = court }
				}
			}
			custom_tooltip = {
				text = child_toy_template_tt
				age < 16
			}
		}
	}

	can_benefit = {
	}

	fallback = {

	}
}

adults_only_template = {
	can_equip = {		
		age > 18
	}

	can_benefit = {
	}

	fallback = {

	}
}
