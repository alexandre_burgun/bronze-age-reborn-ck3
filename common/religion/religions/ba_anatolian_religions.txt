﻿anatolian_religion_group = {
	family = rf_all

	doctrine = ba_default_hostility_doctrine

	#Main Group
	doctrine = doctrine_gender_male_dominated
	doctrine = doctrine_priest_king_no
	doctrine = doctrine_deification_never

	#Mortuary Practices
	doctrine = doctrine_burial_practice_cremation
	doctrine = doctrine_tomb_type_earth_mound

	#Miscellaneous
	doctrine = doctrine_shaving_free_choice
	doctrine = doctrine_sacred_animal_none

	#Marriage
	doctrine = doctrine_concubines
	doctrine = doctrine_bastardry_none
	doctrine = doctrine_consanguinity_cousins

	#Crimes
	doctrine = doctrine_adultery_men_accepted
	doctrine = doctrine_adultery_women_crime
	doctrine = doctrine_kinslaying_close_kin_crime
	#Infanticide
	doctrine = doctrine_infanticide_none

	#Clerical Functions
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	
	#Syncretism
	doctrine = special_doctrine_is_anatolian_faith

	traits = {
		virtues = { brave just diligent }
		sins = { craven arbitrary lazy }
	}

	custom_faith_icons = {
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = {
		{ name = "holy_order_guardians_of_divinerealm" }
		{ name = "holy_order_faithful_of_highgod" }
		{ name = "holy_order_warriors_of_the_symbol" }
	}

	holy_order_maa = { bowmen }

	localization = {
		HighGodName = anatolian_high_god_name
		HighGodNamePossessive = anatolian_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_HE
		HighGodHerselfHimself = CHARACTER_HIMSELF
		HighGodHerHis = CHARACTER_HERHIS_HIS
		HighGodNameAlternate = anatolian_high_god_name_alternate
		HighGodNameAlternatePossessive = anatolian_high_god_name_alternate_possessive

		#Creator
		CreatorName = anatolian_high_god_name
		CreatorNamePossessive = anatolian_high_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_HE
		CreatorHerHis = CHARACTER_HERHIS_HIS
		CreatorHerHim = CHARACTER_HERHIM_HIM

		#HealthGod
		HealthGodName = anatolian_health_god_name
		HealthGodNamePossessive = anatolian_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_HE
		HealthGodHerHis = CHARACTER_HERHIS_HIS
		HealthGodHerHim = CHARACTER_HERHIM_HIM
		
		#FertilityGod
		FertilityGodName = anatolian_fertility_god_name
		FertilityGodNamePossessive = anatolian_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_SHE
		FertilityGodHerHis = CHARACTER_HERHIS_HER
		FertilityGodHerHim = CHARACTER_HERHIM_HER

		#WealthGod
		WealthGodName = anatolian_wealth_god_name
		WealthGodNamePossessive = anatolian_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_SHE
		WealthGodHerHis = CHARACTER_HERHIS_HER
		WealthGodHerHim = CHARACTER_HERHIM_HER

		#HouseholdGod
		HouseholdGodName = anatolian_wealth_god_name
		HouseholdGodNamePossessive = anatolian_wealth_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_SHE
		HouseholdGodHerHis = CHARACTER_HERHIS_HER
		HouseholdGodHerHim = CHARACTER_HERHIM_HER

		#FateGod
		FateGodName = anatolian_high_god_name
		FateGodNamePossessive = anatolian_high_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_HE
		FateGodHerHis = CHARACTER_HERHIS_HIS
		FateGodHerHim = CHARACTER_HERHIM_HIM

		#KnowledgeGod
		KnowledgeGodName = anatolian_knowledge_god_name
		KnowledgeGodNamePossessive = anatolian_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_HE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HIS
		KnowledgeGodHerHim = CHARACTER_HERHIM_HIM

		#WarGod
		WarGodName = anatolian_war_god_name
		WarGodNamePossessive = anatolian_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_HE
		WarGodHerHis = CHARACTER_HERHIS_HIS
		WarGodHerHim = CHARACTER_HERHIM_HIM

		#TricksterGod
		TricksterGodName = anatolian_knowledge_god_name
		TricksterGodNamePossessive = anatolian_knowledge_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = anatolian_night_god_name
		NightGodNamePossessive = anatolian_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_SHE
		NightGodHerHis = CHARACTER_HERHIS_HER
		NightGodHerHim = CHARACTER_HERHIM_HER

		#WaterGod
		WaterGodName = anatolian_water_god_name
		WaterGodNamePossessive = anatolian_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_IT
		WaterGodHerHis = CHARACTER_HERHIS_ITS
		WaterGodHerHim = CHARACTER_HERHIM_IT


		PantheonTerm = anatolian_pantheon
		PantheonTermHasHave = pantheon_term_have
		GoodGodNames = { anatolian_high_god_name anatolian_high_god_name_alternate anatolian_health_god_name anatolian_knowledge_god_name }
		DevilName = paganism_evil_god_decay
		DevilNamePossessive = paganism_evil_god_decay_possessive
		DevilSheHe = CHARACTER_SHEHE_IT
		DevilHerHis = CHARACTER_HERHIS_ITS
		DevilHerselfHimself = CHARACTER_ITSELF
		EvilGodNames = { paganism_evil_god_decay paganism_evil_god_drought paganism_devil_name }
		HouseOfWorship = paganism_house_of_worship
		HouseOfWorshipPlural = paganism_house_of_worship_plural
		ReligiousSymbol = akom_gye_nyame
		ReligiousText = paganism_religious_text
		ReligiousHeadName = anatolian_religious_head
		ReligiousHeadTitleName = paganism_religious_head_title_name
		DevoteeMale = paganism_devotee
		DevoteeMalePlural = paganism_devoteeplural
		DevoteeFemale = paganism_devotee
		DevoteeFemalePlural = paganism_devoteeplural
		DevoteeNeuter = paganism_devotee
		DevoteeNeuterPlural = paganism_devoteeplural
		PriestMale = ba_generic_priest_name
		PriestMalePlural = ba_generic_priest_name_plural
		PriestFemale = ba_generic_priestess_name
		PriestFemalePlural = ba_generic_priestess_name_plural
		PriestNeuter = ba_generic_priest_name
		PriestNeuterPlural = ba_generic_priest_name_plural
		AltPriestTermPlural = ba_generic_priest_name_plural
		BishopMale = ba_generic_high_priest_name
		BishopMalePlural = ba_generic_high_priest_nameplural
		BishopFemale = ba_generic_high_priestess_name
		BishopFemalePlural = ba_generic_high_priestess_name_plural
		BishopNeuter = ba_generic_high_priest_name
		BishopNeuterPlural = ba_generic_high_priest_name_plural
		DivineRealm = anatolian_divine_realm
		PositiveAfterLife = anatolian_afterlife
		NegativeAfterLife = anatolian_afterlife
		DeathDeityName = paganism_death_deity_name
		DeathDeityNamePossessive = paganism_death_deity_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_IT
		DeathDeityHerHis = CHARACTER_HERHIS_ITS
		WitchGodName = anatolian_night_god_name
		WitchGodHerHis = CHARACTER_HERHIS_HER
		WitchGodSheHe = CHARACTER_SHEHE_SHE
		WitchGodHerHim = CHARACTER_HERHIM_HER
		WitchGodMistressMaster = mistress
		WitchGodMotherFather = mother

		GHWName = ghw_great_holy_war
		GHWNamePlural = ghw_great_holy_wars
	}	

	faiths = {
		anatolian_religion = { #actually luwian...
			color = hsv { 0.8 0.35 0.5 }
			icon = custom_faith_9

			doctrine = special_doctrine_is_luwian_faith

			holy_site = tarhuntassa
			holy_site = tarsa
			holy_site = wiyanawanda
			holy_site = millawanda
			holy_site = wilion

			doctrine = tenet_patron_deity
			doctrine = tenet_ritual_celebrations
			doctrine = tenet_astrology
		}
		cult_of_tiwad = {
			color = hsv { 0.8 0.8 0.5 }
			icon = custom_faith_9

			doctrine = special_doctrine_is_luwian_faith

			holy_site = tarhuntassa
			holy_site = tarsa
			holy_site = wiyanawanda
			holy_site = millawanda
			holy_site = wilion

			doctrine = tenet_patron_deity
			doctrine = tenet_ritual_celebrations
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_the_sun
			doctrine = doctrine_deity_tiwad
		}
		cult_of_shanta = {
			color = hsv { 0.8 0.8 0.6 }
			icon = custom_faith_9

			doctrine = special_doctrine_is_luwian_faith

			holy_site = tarhuntassa
			holy_site = tarsa
			holy_site = wiyanawanda
			holy_site = millawanda
			holy_site = wilion

			doctrine = tenet_patron_deity
			doctrine = tenet_ritual_celebrations
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_the_underworld
			doctrine = doctrine_deity_shanta
		}
		cult_of_runtiya = {
			color = hsv { 0.8 0.8 1 }
			icon = custom_faith_9

			doctrine = special_doctrine_is_luwian_faith

			holy_site = tarhuntassa
			holy_site = tarsa
			holy_site = wiyanawanda
			holy_site = millawanda
			holy_site = wilion

			doctrine = tenet_patron_deity
			doctrine = tenet_ritual_celebrations
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_war
			doctrine = doctrine_deity_runtiya
		}
		cult_of_tarhunz = {
			color = hsv { 0.8 0.75 0.7 }
			icon = custom_faith_9

			doctrine = special_doctrine_is_luwian_faith

			holy_site = tarhuntassa
			holy_site = tarsa
			holy_site = wiyanawanda
			holy_site = millawanda
			holy_site = wilion

			doctrine = tenet_patron_deity
			doctrine = tenet_ritual_celebrations
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_storm
			doctrine = doctrine_deity_tarhunz
		}
		cult_of_arma = {
			color = hsv { 0.8 0.8 0.4 }
			icon = custom_faith_9

			doctrine = special_doctrine_is_luwian_faith

			holy_site = tarhuntassa
			holy_site = tarsa
			holy_site = wiyanawanda
			holy_site = millawanda
			holy_site = wilion

			doctrine = tenet_patron_deity
			doctrine = tenet_ritual_celebrations
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_the_moon
			doctrine = doctrine_deity_arma
		}
		cult_of_apaliunas = {
			color = hsv { 0.8 0.75 0.6 }
			icon = custom_faith_9

			doctrine = special_doctrine_is_luwian_faith

			holy_site = tarhuntassa
			holy_site = tarsa
			holy_site = wiyanawanda
			holy_site = millawanda
			holy_site = wilion

			doctrine = tenet_patron_deity
			doctrine = tenet_ritual_celebrations
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_the_underworld
			doctrine = doctrine_deity_apaliunas
		}
		cult_of_hapantali = {
			color = hsv { 0.8 0.8 0.9 }
			icon = custom_faith_9

			doctrine = special_doctrine_is_luwian_faith

			holy_site = tarhuntassa
			holy_site = tarsa
			holy_site = wiyanawanda
			holy_site = millawanda
			holy_site = wilion

			doctrine = tenet_patron_deity
			doctrine = tenet_ritual_celebrations
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_fertility
			doctrine = doctrine_deity_hapantali
		}
		cult_of_ala = {
			color = hsv { 0.8 0.8 0.8 }
			icon = custom_faith_9

			doctrine = special_doctrine_is_luwian_faith

			holy_site = tarhuntassa
			holy_site = tarsa
			holy_site = wiyanawanda
			holy_site = millawanda
			holy_site = wilion

			doctrine = tenet_patron_deity
			doctrine = tenet_ritual_celebrations
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_fertility
			doctrine = doctrine_deity_ala
		}
		hittite_religion = {
			color = hsv { 0.7 0.35 0.5 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology
		}
		cult_of_arinna = {
			color = hsv { 0.7 0.8 0.5 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_the_sun
			doctrine = doctrine_deity_arinna
		}
		cult_of_anzili = {
			color = hsv { 0.7 0.8 0.9 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_fertility
			doctrine = doctrine_deity_anzili
		}
		cult_of_kamrusepa = {
			color = hsv { 0.7 0.75 0.9 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_fertility
			doctrine = doctrine_deity_kamrusepa
		}
		cult_of_aruna = {
			color = hsv { 0.7 0.8 0.8 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_farming
			doctrine = doctrine_deity_aruna
		}
		cult_of_ellel = {
			color = hsv { 0.7 0.8 0.2 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_state
			doctrine = doctrine_deity_ellel
		}
		cult_of_halki = {
			color = hsv { 0.7 0.85 0.8 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_farming
			doctrine = doctrine_deity_halki
		}
		cult_of_hanwasuit = {
			color = hsv { 0.7 0.75 0.2 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_state
			doctrine = doctrine_deity_hanwasuit
		}
		cult_of_tarawa = {
			color = hsv { 0.7 0.85 0.9 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_fertility
			doctrine = doctrine_deity_tarawa
		}
		cult_of_nerik = {
			color = hsv { 0.7 0.8 0.7 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_storm
			doctrine = doctrine_deity_nerik
		}
		cult_of_sharissa = {
			color = hsv { 0.7 0.8 0.7 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_storm
			doctrine = doctrine_deity_sharissa
		}
		cult_of_suwaliyat = {
			color = hsv { 0.7 0.8 1 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_war
			doctrine = doctrine_deity_suwaliyat
		}
		cult_of_zippalanda = {
			color = hsv { 0.7 0.75 0.7 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_storm
			doctrine = doctrine_deity_zippalanda
		}
		cult_of_alalu = {
			color = hsv { 0.7 0.82 0.9 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_fertility
			doctrine = doctrine_deity_alalu
		}
		cult_of_inara_hittite = {
			color = hsv { 0.7 0.81 0.9 }
			icon = hittite

			doctrine = special_doctrine_is_hittite_faith

			#Main Group
			doctrine = doctrine_priest_king_yes
			doctrine = doctrine_deification_dead_only

			#Mortuary Practices
			doctrine = doctrine_burial_practice_burial
			doctrine = doctrine_tomb_type_chamber_tomb

			#Miscellaneous
			doctrine = doctrine_shaving_always_have_beard

			holy_site = hattusha
			holy_site = kussara
			holy_site = halab
			holy_site = kanesh
			holy_site = tuwanuwa

			doctrine = tenet_hattian_syncretism
			doctrine = tenet_warmonger
			doctrine = tenet_astrology

			doctrine = tenet_cult_of_fertility
			doctrine = doctrine_deity_inara
		}
	}
}