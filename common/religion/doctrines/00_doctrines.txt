﻿doctrine_marriage_type = {
	group = "marriage"
	doctrine_monogamy = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_monogamy }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			number_of_spouses = 1
			marriage_event = yes
		}
	}

	doctrine_polygamy = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_polygamy }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			number_of_spouses = 4
			spouse_piety_loss = yes
		}
	}

	doctrine_concubines = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_concubines }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			number_of_spouses = 1
			number_of_consorts = 3
		}
	}
}

doctrine_bastardry = {
	group = "marriage"
	doctrine_bastardry_none = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_bastardry_none }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_bastardry_all }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		parameters = {
			bastards_none = yes
		}
	}
	doctrine_bastardry_legitimization = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_bastardry_legitimization }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			bastards_legitimize = yes
		}
	}
	doctrine_bastardry_all = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_bastardry_all }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_bastardry_none }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		parameters = {
			bastards_always = yes
		}
	}
}

doctrine_adultery_men = {
	group = "crimes"
	doctrine_adultery_men_crime = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_adultery_men_crime }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_adultery_men_accepted }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		parameters = {
			adultery_male_crime = yes
		}
	}
	doctrine_adultery_men_shunned = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_adultery_men_shunned }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			adultery_male_shunned = yes
		}
	}
	doctrine_adultery_men_accepted = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_adultery_men_accepted }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_adultery_men_crime }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		parameters = {
			adultery_male_accepted = yes
		}
	}
}

doctrine_adultery_women = {
	group = "crimes"
	doctrine_adultery_women_crime = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_adultery_women_crime }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_adultery_women_accepted }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		parameters = {
			adultery_female_crime = yes
		}
	}
	doctrine_adultery_women_shunned = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_adultery_women_shunned }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			adultery_female_shunned = yes
		}
	}
	doctrine_adultery_women_accepted = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_adultery_women_accepted }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_adultery_women_crime }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		parameters = {
			adultery_female_accepted = yes
		}
	}
}

doctrine_kinslaying = {
	group = "crimes"
	doctrine_kinslaying_any_dynasty_member_crime = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_kinslaying_any_dynasty_member_crime }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_kinslaying_close_kin_crime }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
			else_if = {
				limit = { has_doctrine = doctrine_kinslaying_shunned }
				multiply = faith_changed_doctrine_cost_mult_three_step
			}
			else_if = {
				limit = { has_doctrine = doctrine_kinslaying_shunned }
				multiply = faith_changed_doctrine_cost_mult_four_step
			}
		}
		parameters = {
			kinslaying_any_dynasty_member_crime = yes
		}
	}
	doctrine_kinslaying_extended_family_crime = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_kinslaying_extended_family_crime }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_kinslaying_shunned }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
			else_if = {
				limit = { has_doctrine = doctrine_kinslaying_accepted }
				multiply = faith_changed_doctrine_cost_mult_three_step
			}
		}
		parameters = {
			kinslaying_extended_family_crime = yes
		}
	}
	doctrine_kinslaying_close_kin_crime = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_kinslaying_close_kin_crime }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = {
					OR = {
						has_doctrine = doctrine_kinslaying_accepted
						has_doctrine = doctrine_kinslaying_any_dynasty_member_crime
					}
				}
			}
			multiply = faith_changed_doctrine_cost_mult_two_step
		}
		parameters = {
			kinslaying_close_kin_crime = yes
		}
	}
	doctrine_kinslaying_shunned = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_kinslaying_shunned }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_kinslaying_extended_family_crime }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
			else_if = {
				limit = { has_doctrine = doctrine_kinslaying_any_dynasty_member_crime }
				multiply = faith_changed_doctrine_cost_mult_three_step
			}
		}
		parameters = {
			kinslaying_shunned = yes
		}
	}
	doctrine_kinslaying_accepted = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_kinslaying_accepted }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_kinslaying_close_kin_crime }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
			else_if = {
				limit = { has_doctrine = doctrine_kinslaying_extended_family_crime }
				multiply = faith_changed_doctrine_cost_mult_three_step
			}
			else_if = {
				limit = { has_doctrine = doctrine_kinslaying_any_dynasty_member_crime }
				multiply = faith_changed_doctrine_cost_mult_four_step
			}

		}
		parameters = {
			kinslaying_accepted = yes
		}
	}
}


doctrine_gender = {
	group = "main_group"

	doctrine_gender_male_dominated = {
		is_shown = {
			NOT = { has_game_rule = full_gender_equality }
		}
		piety_cost = {
			value = faith_doctrine_cost_high
			if = {
				limit = { has_doctrine = doctrine_gender_male_dominated }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_gender_female_dominated }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		parameters = {
			male_dominated_law = yes
			female_claims_are_weak = yes
			women_cannot_inherit_claims = yes
			women_cannot_be_granted_titles = yes
			combatant_must_be_male = yes
			male_dominated_council = yes
		}
		character_modifier = {
			name = female_ruler_male_dominant
			opinion_of_female_rulers = -10
		}
	}
	doctrine_gender_equal = {
		piety_cost = {
			value = faith_doctrine_cost_high
			if = {
				limit = { has_doctrine = doctrine_gender_equal }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			gender_equal_law = yes
			combatant_can_be_either_gender = yes
		}
	}
	doctrine_gender_equal_male_warriors = {
		piety_cost = {
			value = faith_doctrine_cost_high
			if = {
				limit = { has_doctrine = doctrine_gender_equal }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			gender_equal_law = yes
			combatant_must_be_male = yes
		}
	}
	doctrine_gender_eblaite_equality = {
		piety_cost = {
			value = faith_doctrine_cost_high
			if = {
				limit = { has_doctrine = doctrine_gender_eblaite_equality }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			male_dominated_law = yes
			female_claims_are_weak = yes
			women_cannot_inherit_claims = yes
			#women_cannot_be_granted_titles = yes
			combatant_must_be_male = yes
			#male_dominated_council = yes
		}
		character_modifier = {
			name = female_ruler_male_dominant
			opinion_of_female_rulers = -10
		}
	}
	doctrine_gender_minoan_matriarchy = {
		piety_cost = {
			value = faith_doctrine_cost_high
			if = {
				limit = { has_doctrine = doctrine_gender_minoan_matriarchy }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			female_dominated_law = yes
			male_claims_are_weak = yes
			men_cannot_inherit_claims = yes
			men_cannot_be_granted_titles = yes
			combatant_must_be_male = yes
			female_dominated_council = yes
		}
		character_modifier = {
			name = male_ruler_female_dominant
			opinion_of_male_rulers = -10
		}
	}
	doctrine_gender_female_dominated = {
		is_shown = {
			NOT = { has_game_rule = full_gender_equality }
		}
		piety_cost = {
			value = faith_doctrine_cost_high
			if = {
				limit = { has_doctrine = doctrine_gender_female_dominated }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_gender_male_dominated }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		parameters = {
			female_dominated_law = yes
			male_claims_are_weak = yes
			men_cannot_inherit_claims = yes
			men_cannot_be_granted_titles = yes
			combatant_must_be_female = yes
			female_dominated_council = yes
		}
		character_modifier = {
			name = male_ruler_female_dominant
			opinion_of_male_rulers = -10
		}
	}
}

doctrine_consanguinity = {
	group = "marriage"
	doctrine_consanguinity_restricted = {
		piety_cost = {
			value = faith_doctrine_cost_high
			if = {
				limit = { has_doctrine = doctrine_consanguinity_restricted }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_consanguinity_aunt_nephew_and_uncle_niece }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
			else_if = {
				limit = { has_doctrine = doctrine_consanguinity_unrestricted }
				multiply = faith_changed_doctrine_cost_mult_three_step
			}
		}
		parameters = {
			consanguinity_restricted_marriage = yes
			consanguinity_restricted_close_kin_incest = yes
		}
	}
	doctrine_consanguinity_cousins = {
		piety_cost = {
			value = faith_doctrine_cost_high
			if = {
				limit = { has_doctrine = doctrine_consanguinity_cousins }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_consanguinity_unrestricted }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		parameters = {
			consanguinity_cousins_marriage = yes
			consanguinity_cousins_incest = yes
			allows_cousin_marriage = yes
		}
	}
	doctrine_consanguinity_aunt_nephew_and_uncle_niece = {
		piety_cost = {
			value = faith_doctrine_cost_high
			if = {
				limit = { has_doctrine = doctrine_consanguinity_aunt_nephew_and_uncle_niece }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_consanguinity_restricted }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		parameters = {
			consanguinity_aunt_nephew_and_uncle_niece_marriage = yes
			consanguinity_aunt_nephew_and_uncle_niece_incest = yes
			allows_cousin_marriage = yes
			allows_aunt_nephew_and_uncle_niece_marriage = yes
		}
	}
	doctrine_consanguinity_unrestricted = {
		piety_cost = {
			value = faith_doctrine_cost_high
			if = {
				limit = { has_doctrine = doctrine_consanguinity_unrestricted }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_consanguinity_cousins }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
			else_if = {
				limit = { has_doctrine = doctrine_consanguinity_restricted }
				multiply = faith_changed_doctrine_cost_mult_three_step
			}
		}
		parameters = {
			consanguinity_unrestricted_marriage = yes
			consanguinity_unrestricted_incest = yes
			allows_cousin_marriage = yes
			allows_aunt_nephew_and_uncle_niece_marriage = yes
			allows_unrestricted_marriage = yes
		}
	}
}

doctrine_clerical_gender = {
	group = "clergy"
	doctrine_clerical_gender_male_only = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_clerical_gender_male_only }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_clerical_gender_female_only }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		parameters = {
			clergy_must_be_male = yes
		}
	}
	doctrine_clerical_gender_female_only = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_clerical_gender_female_only }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_clerical_gender_male_only }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		parameters = {
			clergy_must_be_female = yes
		}
	}
	doctrine_clerical_gender_either = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_clerical_gender_either }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}

		parameters = {
			clergy_can_be_either_gender = yes
		}
	}
}

doctrine_clerical_marriage = {
	group = "clergy"
	doctrine_clerical_marriage_allowed = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_clerical_marriage_allowed }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_clerical_marriage_disallowed }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}
		
		parameters = {
			clergy_can_marry = yes
		}
	}
	doctrine_clerical_marriage_disallowed = {
		piety_cost = {
			value = faith_doctrine_cost_low
			if = {
				limit = { has_doctrine = doctrine_clerical_marriage_disallowed }
				multiply = faith_unchanged_doctrine_cost_mult
			}
			else_if = {
				limit = { has_doctrine = doctrine_clerical_marriage_allowed }
				multiply = faith_changed_doctrine_cost_mult_two_step
			}
		}

		parameters = {
			clergy_can_marry = no
		}
	}
}
