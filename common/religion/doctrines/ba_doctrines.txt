﻿doctrine_infanticide = {
	group = "crimes"
	doctrine_infanticide_none = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_infanticide_none }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			infanticide_none = yes
		}
	}
	doctrine_infanticide_any = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_infanticide_any }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			infanticide_any = yes
		}
	}
}

doctrine_priest_king = {
	group = "main_group"
	doctrine_priest_king_pharaoh = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_priest_king_pharaoh }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			priest_king_pharaoh = yes
			clerical_appointment_ruler = yes
			clerical_appointment_fixed = no
		}
	}
	doctrine_priest_king_yes = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_priest_king_yes }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			priest_king = yes
			clerical_appointment_ruler = yes
			clerical_appointment_fixed = no
		}
	}
	doctrine_priest_king_no = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_priest_king_no }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			priest_king = no
			clerical_appointment_head_of_faith = yes
			clerical_appointment_fixed = no
		}
	}
}

doctrine_deification = {
	group = "main_group"
	doctrine_deification_alive_and_dead = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_deification_alive_and_dead }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			deification_alive_and_dead = yes
		}
	}
	doctrine_deification_dead_only = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_deification_dead_only }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			deification_dead_only = yes
		}
	}
	doctrine_deification_dead_exceptional_only = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_deification_dead_exceptional_only }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			deification_dead_exceptional_only = yes
		}
	}
	doctrine_deification_never = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_deification_never }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
		}
	}
}

doctrine_burial_practice = {
	group = "mortuary_practice"
	doctrine_burial_practice_horse_burial = {
		icon = doctrine_burial_practice
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_burial_practice_horse_burial }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			burial_practice_horse_burial = yes
		}
	}
	doctrine_burial_practice_mummification = {
		icon = doctrine_burial_practice
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_burial_practice_mummification }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			burial_practice_mummification = yes
		}
	}
	doctrine_burial_practice_cremation = {
		icon = doctrine_burial_practice
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_burial_practice_cremation }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			burial_practice_cremation = yes
		}
	}
	doctrine_burial_practice_burial = {
		icon = doctrine_burial_practice
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_burial_practice_burial }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			burial_practice_burial = yes
		}
	}
}

doctrine_tomb_type = {
	group = "mortuary_practice"
	doctrine_tomb_type_pyramids = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_tomb_type_pyramids }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			tomb_type_pyramid = yes
		}
	}
	doctrine_tomb_type_earth_mound = { #Burial Mound
		icon = doctrine_tomb_type
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_tomb_type_earth_mound }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			tomb_type_earth_mound = yes
		}
	}
	doctrine_tomb_type_cist_tomb = {
		icon = doctrine_tomb_type
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_tomb_type_cist_tomb }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			tomb_type_cist_tomb = yes
		}
	}
	doctrine_tomb_type_chamber_tomb = {
		icon = doctrine_tomb_type
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_tomb_type_chamber_tomb }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			tomb_type_chamber_tomb = yes
		}
	}
}

doctrine_shaving = {
	group = "miscellaneous"
	doctrine_shaving_all_and_whigs = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_shaving_all_and_whigs }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			never_beard = yes
			egyptian_fake_beard = yes
		}
	}
	doctrine_shaving_never_beard = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_shaving_never_beard }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			never_beard = yes
		}
	}
	doctrine_shaving_free_choice = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_shaving_free_choice }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			#shaving_free_choice = yes
		}
	}
	doctrine_shaving_always_have_beard = {
		piety_cost = {
			value = faith_doctrine_cost_mid
			if = {
				limit = { has_doctrine = doctrine_shaving_always_have_beard }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			always_have_beard = yes
		}
	}
}

doctrine_sacred_animal = {
	group = "miscellaneous"
	doctrine_sacred_animal_cat = {
		piety_cost = {
			value = 75
			if = {
				limit = { has_doctrine = doctrine_sacred_animal_cat }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			sacred_animal_cat = yes
		}
	}
	doctrine_sacred_animal_crocodile = {
		piety_cost = {
			value = 75
			if = {
				limit = { has_doctrine = doctrine_sacred_animal_crocodile }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
			sacred_animal_crocodile = yes
		}
	}
	doctrine_sacred_animal_none = {
		piety_cost = {
			value = 75
			if = {
				limit = { has_doctrine = doctrine_sacred_animal_none }
				multiply = faith_unchanged_doctrine_cost_mult
			}
		}
		parameters = {
		}
	}
}