﻿is_egyptian_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_egyptian_faith
	}

	special_doctrine_is_egyptian_faith = {
		visible = no
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Christian faiths are given this special doctrine which makes them friendlier to Faiths with the Christian Syncretism tenet.
			hostility_override_tenet_egyptian_syncretism = 1
			access_to_egyptian_deities = yes
			#christian_syncretic_recipient_opinion_active = yes
			#opinion_of_christian_syncretic_actor_opinion_active = 30
		}
	}
}

is_aegean_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_aegean_faith
	}

	special_doctrine_is_aegean_faith = {
		visible = no
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Christian faiths are given this special doctrine which makes them friendlier to Faiths with the Christian Syncretism tenet.
			hostility_override_tenet_aegean_syncretism = 1
			access_to_aegean_deities = yes
			#christian_syncretic_recipient_opinion_active = yes
			#opinion_of_christian_syncretic_actor_opinion_active = 30
		}
	}
}

is_mesopotamian_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_mesopotamian_faith
	}

	special_doctrine_is_mesopotamian_faith = {
		visible = no
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Christian faiths are given this special doctrine which makes them friendlier to Faiths with the Christian Syncretism tenet.
			hostility_override_tenet_mesopotamian_syncretism = 1
			hostility_override_tenet_sumerian_mythology = 0
			access_to_mesopotamian_deities = yes
			#christian_syncretic_recipient_opinion_active = yes
			#opinion_of_christian_syncretic_actor_opinion_active = 30
		}
	}
}

is_hurrian_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_hurrian_faith
	}

	special_doctrine_is_hurrian_faith = {
		visible = no
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Christian faiths are given this special doctrine which makes them friendlier to Faiths with the Christian Syncretism tenet.
			hostility_override_tenet_hurrian_syncretism = 1
			access_to_hurrian_deities = yes
			#christian_syncretic_recipient_opinion_active = yes
			#opinion_of_christian_syncretic_actor_opinion_active = 30
		}
	}
}

is_hellenic_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_hellenic_faith
	}

	special_doctrine_is_hellenic_faith = {
		visible = no
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Christian faiths are given this special doctrine which makes them friendlier to Faiths with the Christian Syncretism tenet.
			hostility_override_tenet_hellenic_syncretism = 1
			access_to_hellenic_deities = yes
			#christian_syncretic_recipient_opinion_active = yes
			#opinion_of_christian_syncretic_actor_opinion_active = 30
		}
	}
}

is_levantine_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_levantine_faith
	}

	special_doctrine_is_levantine_faith = {
		visible = no
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Christian faiths are given this special doctrine which makes them friendlier to Faiths with the Christian Syncretism tenet.
			hostility_override_tenet_levantine_syncretism = 1
			access_to_levantine_deities = yes
			#christian_syncretic_recipient_opinion_active = yes
			#opinion_of_christian_syncretic_actor_opinion_active = 30
		}
	}
}

is_hattian_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_hattian_faith
	}

	special_doctrine_is_hattian_faith = {
		visible = no
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Christian faiths are given this special doctrine which makes them friendlier to Faiths with the Christian Syncretism tenet.
			hostility_override_tenet_hattian_syncretism = 1
			access_to_hattian_deities = yes
			#christian_syncretic_recipient_opinion_active = yes
			#opinion_of_christian_syncretic_actor_opinion_active = 30
		}
	}
}

is_elamite_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_elamite_faith
	}

	special_doctrine_is_elamite_faith = {
		visible = no
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Christian faiths are given this special doctrine which makes them friendlier to Faiths with the Christian Syncretism tenet.
			hostility_override_tenet_elamite_syncretism = 1
			access_to_elamite_deities = yes
			#christian_syncretic_recipient_opinion_active = yes
			#opinion_of_christian_syncretic_actor_opinion_active = 30
		}
	}
}

is_anatolian_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_anatolian_faith
	}

	special_doctrine_is_anatolian_faith = {
		visible = no
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Christian faiths are given this special doctrine which makes them friendlier to Faiths with the Christian Syncretism tenet.
			hostility_override_tenet_anatolian_syncretism = 1
			access_to_anatolian_deities = yes
			#christian_syncretic_recipient_opinion_active = yes
			#opinion_of_christian_syncretic_actor_opinion_active = 30
		}
	}
}

is_kushite_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_kushite_faith
	}

	special_doctrine_is_kushite_faith = {
		visible = no
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Christian faiths are given this special doctrine which makes them friendlier to Faiths with the Christian Syncretism tenet.
			hostility_override_tenet_kushite_syncretism = 1
			access_to_kushite_deities = yes
			#christian_syncretic_recipient_opinion_active = yes
			#opinion_of_christian_syncretic_actor_opinion_active = 30
		}
	}
}

is_zagrosian_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_zagrosian_faith
	}

	special_doctrine_is_zagrosian_faith = {
		visible = no
		parameters = {
			# Since doctrines cannot modify other faith's views of us, only our view of others, all Christian faiths are given this special doctrine which makes them friendlier to Faiths with the Christian Syncretism tenet.
			hostility_override_tenet_zagrosian_syncretism = 1
			access_to_zagrosian_deities = yes
		}
	}
}

is_akkadian_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_akkadian_faith
	}

	special_doctrine_is_akkadian_faith = {
		visible = no
		parameters = {
			access_to_akkadian_deities = yes
		}
	}
}

is_sumerian_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_sumerian_faith
	}

	special_doctrine_is_sumerian_faith = {
		visible = no
		parameters = {
			access_to_sumerian_deities = yes
		}
	}
}

is_north_levantine_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_north_levantine_faith
	}

	special_doctrine_is_north_levantine_faith = {
		visible = no
		parameters = {
			access_to_north_levantine_deities = yes
		}
	}
}

is_canaanite_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_canaanite_faith
	}

	special_doctrine_is_canaanite_faith = {
		visible = no
		parameters = {
			access_to_canaanite_deities = yes
		}
	}
}

is_phoenician_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_phoenician_faith
	}

	special_doctrine_is_phoenician_faith = {
		visible = no
		parameters = {
			access_to_phoenician_deities = yes
		}
	}
}

is_luwian_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_luwian_faith
	}

	special_doctrine_is_luwian_faith = {
		visible = no
		parameters = {
			access_to_luwian_deities = yes
		}
	}
}

is_hittite_faith = {
	group = "special"
	
	is_available_on_create = {
		has_doctrine = special_doctrine_is_hittite_faith
	}

	special_doctrine_is_hittite_faith = {
		visible = no
		parameters = {
			access_to_hittite_deities = yes
		}
	}
}

nudity_doctrine = {
	group = "special"
	is_available_on_create = {
		has_doctrine = special_doctrine_naked_priests
	}

	special_doctrine_naked_priests = {
		parameters = {
			naked_priests_active = yes
		}
	}
}

divine_destiny = {
	group = "special"
	is_available_on_create = {
		has_doctrine = divine_destiny_doctrine
	}
	divine_destiny_doctrine = {
		piety_cost = {
			value = faith_doctrine_cost_low
		}
		parameters = {
			divine_destiny_holy_war_cost_reduction = yes
		}
	}
}


#Game rule only
full_tolerance = {
	group = "special"
	is_available_on_create = {
		has_doctrine = special_doctrine_full_tolerance
	}
	
	special_doctrine_full_tolerance = {
		parameters = {
			hostility_override_special_doctrine_full_tolerance = 0
		}
	}
}

