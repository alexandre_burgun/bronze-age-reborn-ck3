﻿tributary_government = {
	primary_holding = settlement_holding
	valid_holdings = { city_holding }
	create_cadet_branches = yes
	rulers_should_have_dynasty = yes
	dynasty_named_realms = yes
	royal_court = yes

	#required_county_holdings = { settlement_holding }

	vassal_contract = {
		tributary_status
		special_contract
		council_rights
	}
	
	color = hsv{ 0.34 0.54 0.78 }
}
palatial_government = {
	primary_holding = city_holding
	valid_holdings = { settlement_holding }
	create_cadet_branches = yes
	rulers_should_have_dynasty = yes
	dynasty_named_realms = yes
	royal_court = yes

	#required_county_holdings = { city_holding }
	can_get_government = {
		character_can_be_palatial_trigger = yes
	}
	vassal_contract = {
		normal_vassal_obligations
		feudal_government_taxes
		feudal_government_levies
		special_contract
		religious_rights
		fortification_rights
		succession_rights
		war_declaration_rights
		council_rights
		title_revocation_rights
	}
	character_modifier = {
		diplomatic_range_mult = 0.5
	}
	
	color = hsv{ 0.54 1.00 0.58 }
}