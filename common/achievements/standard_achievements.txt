﻿until_death_do_us_part_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

way_of_life_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

a_legacy_to_last_the_ages_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

a_house_of_my_own_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

dreadful_ruler_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

stressful_situation_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

not_so_feudal_system_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

the_succession_is_safe_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

almost_there_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

last_count_first_king_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

prolific_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

a_name_known_throughout_the_world_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

end_of_an_era_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

turning_to_diamonds_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

reconquista_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

seven_holy_cities_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

frankokratia_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

celebrity_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

saint_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

keeping_it_in_the_family_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

non_nobis_domine_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

trapped_in_the_web_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

followed_by_shadows_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

what_nepotism_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

rise_from_the_ashes_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

the_emerald_isle_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

from_rags_to_riches_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

give_a_dog_a_bone_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

al_andalus_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

wily_as_the_fox_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

sibling_rivalry_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

blood_eagle_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

kings_to_the_seventh_generation_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

norman_yoke_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

royal_dignity_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

going_places_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

land_of_the_rus_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

above_god_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

paragon_of_virtue_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

the_things_love_does_for_us_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

fine_print_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

know_your_place_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

monumental_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

its_not_a_cult_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

the_things_we_do_for_love_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

an_unfortunate_accident_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

death_did_us_part_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

for_the_faith_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

bad_blood_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

seductive_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

the_emperors_new_clothes_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

a_perfect_circle_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

carolingian_consolidation_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

moving_up_in_the_world_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

beacon_of_progress_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}

mother_of_us_all_achievement = {
	possible = {
		always = no
	}
	happened = {
	}
}
