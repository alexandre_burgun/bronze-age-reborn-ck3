﻿ba_found_dynasty_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_dynasty_house.dds"
	major = yes
	ai_check_interval = 60
	desc = ba_found_dynasty_decision_desc

	is_shown = {
		faith = { has_doctrine = tenet_pharaohs }
		highest_held_title_tier = tier_duchy
		#NOT = { has_game_rule = off_custom_kingdoms }
	}

	is_valid = {
		prestige_level >= 3
		piety_level >= 3
		is_independent_ruler = yes
		OR = {
			custom_description = {
				text = found_kingdom_decision_two_duchies_held
				any_held_title = {
					count > 1
					tier = tier_duchy
				}
			}
			sub_realm_size >= 10
		}
	}

	is_valid_showing_failures_only = {
		is_available_adult = yes
		is_at_war = no
	}

	cost = {
		gold = 200
		prestige = 750
		piety = 750
	}

	effect = {
		ba_found_dynasty_effect = yes
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}

ba_reunite_egypt_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_dynasty_house.dds"
	major = yes
	ai_check_interval = 60
	desc = ba_found_dynasty_decision_desc

	is_shown = {
		faith = { has_doctrine = tenet_pharaohs }
		highest_held_title_tier = tier_kingdom
		OR = {
			is_independent_ruler = yes
			any_held_title = {
				count >= 3
				tier = tier_kingdom
			}
		}
		#NOT = { has_game_rule = off_custom_kingdoms }
	}

	is_valid = {
		prestige_level >= 4
		piety_level >= 4
		sub_realm_size >= 80
		title:k_upper_egypt.holder = root
		title:k_lower_egypt.holder = root
		trigger_if = {
			limit = { title:e_kemet = { exists = holder } }
			title:e_kemet.holder = { faith = { has_doctrine = tenet_pharaohs } }
		}
		root.capital_county.empire = title:e_kemet
	}

	is_valid_showing_failures_only = {
		is_independent_ruler = yes
		is_available_adult = yes
		is_at_war = no
	}

	cost = {
		gold = 1200
		prestige = 2000
		piety = 1500
	}

	effect = {
		hidden_effect = {
			save_scope_as = founder
			primary_title = {
				save_scope_as = old_title
			}
		}
		
		create_title_and_vassal_change = {
			type = created
			save_scope_as = change
		}

		title:e_kemet = {
			change_title_holder = {
				holder = root
				change = scope:change
			}
		}

		resolve_title_and_vassal_change = scope:change

		title:e_kemet = {
			set_capital_county = root.capital_county
		}
		set_primary_title_to = title:e_kemet

		trigger_event = major_decisions.1103 #Change to a custom one
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}

ba_restore_babylon_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_dynasty_house.dds"
	major = yes
	ai_check_interval = 180
	desc = ba_restore_babylon_decision_desc

	is_shown = {
		has_global_variable = second_startdate
		title:c_babylon.holder = root
		NOT = { has_global_variable = babylon_restored }
	}

	is_valid = {
		custom_description = {
			text = marduk_statue_returned_tooltip
			has_global_variable = marduk_statue_returned
		}
		custom_description = {
			text = esagila_restored_tooltip
			province:6087 = {
				OR = {
					has_building = esagila_01
					has_building = esagila_02
				}
			}
		}
	}

	is_valid_showing_failures_only = {
		is_available_adult = yes
		is_at_war = no
	}

	cost = {
		prestige = 500
		piety = 500
	}

	effect = {
		dynasty = { add_dynasty_prestige = 1000 }
		set_global_variable = {
			name = babylon_restored
			value = 1
		}
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}

ba_return_marduk_statue_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_dynasty_house.dds"
	major = yes
	ai_check_interval = 120
	desc = ba_found_dynasty_decision_desc

	is_shown = {
		has_global_variable = second_startdate
		title:c_babylon.holder = root
		OR = {
			title:c_terqa.holder = root
			title:c_terqa.holder.top_liege = root
		}
		NOT = { has_global_variable = babylon_restored }
		NOT = { has_global_variable = marduk_statue_returned }
	}

	is_valid = {
	}

	is_valid_showing_failures_only = {
		is_available_adult = yes
		is_at_war = no
	}

	cost = {
	}

	effect = {
		set_global_variable = {
			name = marduk_statue_returned
			value = 1
		}
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}
restore_nippur_prieshood_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_dynasty_house.dds"
	major = yes
	ai_check_interval = 60
	desc = restore_nippur_prieshood_decision_desc

	is_shown = {
		faith = {
			OR = {
				has_doctrine = tenet_mesopotamian_syncretism
				religion_tag = mesopotamian_religion_group
			}
		}
		NOT = { title:c_nippur.holder = { has_government = theocracy_government } }
	}

	is_valid = {
		title:c_nippur.holder = root
		trigger_if = {
			limit = {
				exists = title:d_nippur.holder
			}
			title:d_nippur.holder = root
		}
		trigger_else = {
			exists = title:d_nippur.holder
		}
	}

	is_valid_showing_failures_only = {
		is_available_adult = yes
		is_at_war = no
		NOT = { capital_county = title:c_nippur}
	}

	cost = {
		gold = 250
		prestige = 750
	}

	effect = {
		save_scope_as = togettitle #error suppression
		if = {
			limit = { dynasty = { NOT = { has_variable = restored_nippur_priesthood } } }
			add_piety = 1500
			dynasty = { add_dynasty_prestige = 1000 }
		}
		else = {
			custom_tooltip = "CAN_ONLY_GET_NIPPUR_REWARD_ONCE_TT"
		}
		if = { #Give to daughter
			limit = {
				NOT = { faith = { has_doctrine_parameter = clergy_must_be_male } }
				any_child = {
					age >= 16
					is_female = yes
					is_married = no
					employer = root
					is_imprisoned = no
				}
			}
			ordered_child = {
				limit = {
					age >= 16
					is_female = yes
					is_married = no
					employer = root
					is_imprisoned = no
				}
				order_by = piety
				position = 0
				save_scope_as = togettitle
				custom_tooltip = "DAUGHTER_RECIEVES_NIPPUR_TT"
			}
		}
		else_if = { #Give to sister
			limit = {
				NOT = { faith = { has_doctrine_parameter = clergy_must_be_male } }
				any_sibling = {
					age >= 16
					is_female = yes
					is_married = no
					employer = root
					is_imprisoned = no
				}
			}
			ordered_sibling = {
				limit = {
					age >= 16
					is_female = yes
					is_married = no
					employer = root
					is_imprisoned = no
				}
				order_by = piety
				position = 0
				save_scope_as = togettitle
				custom_tooltip = "SISTER_RECIEVES_NIPPUR_TT"
			}
		}
		else = { # If no valid daughter or sister, create new character
			custom_tooltip = "NEW_CHARACTER_RECIEVES_NIPPUR_TT"
			if = {
				limit = { faith = { has_doctrine_parameter = clergy_must_be_male } }
				create_character = {
					age = { 25 45 }
					gender_female_chance = 0
					culture = root.culture
					faith = root.faith
					dynasty = none
					location = province:6000
					save_scope_as = togettitle
				}
			}
			else_if = {
				limit = { faith = { has_doctrine_parameter = clergy_must_be_female } }
				create_character = {
					age = { 25 45 }
					gender_female_chance = 100
					culture = root.culture
					faith = root.faith
					dynasty = none
					location = province:6000
					save_scope_as = togettitle
				}
			}
			else = {
				create_character = {
					age = { 25 45 }
					gender_female_chance = 50
					culture = root.culture
					faith = root.faith
					dynasty = none
					location = province:6000
					save_scope_as = togettitle
				}
			}
		}
		create_title_and_vassal_change = {
			type = created
			save_scope_as = change
		}
		every_sub_realm_barony = {
			limit = {
				holder = root
				county = title:c_nippur
			}
			change_title_holder = {
				holder = scope:togettitle
				change = scope:change
			}
		}
		hidden_effect = {
			title:c_nippur = {
				change_title_holder = {
					holder = scope:togettitle
					change = scope:change
				}
			}
			title:d_nippur = {
				change_title_holder = {
					holder = scope:togettitle
					change = scope:change
				}
			}
			scope:togettitle = {
				change_liege = {
					liege = root
					change = scope:change
				}
			}
		}

		resolve_title_and_vassal_change = scope:change
		dynasty = {
			set_variable = {
				name = restored_nippur_priesthood
				value = 1
			}
		}
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}
