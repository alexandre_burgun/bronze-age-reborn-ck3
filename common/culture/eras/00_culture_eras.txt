﻿culture_era_tribal = { #Early bronze Age
	year = 0
}

culture_era_early_medieval = { #Middle Bronze Age
	year = 850
	invalid_for_government = tribal_government
	
	culture_modifier = {
		culture_tradition_max_add = 1
	}
	custom = more_expensive_court_positions
}

culture_era_high_medieval = { #Late Bronze Age
	year = 1150
	invalid_for_government = tribal_government
	
	culture_modifier = {
		culture_tradition_max_add = 1
	}
	
	modifier = {
		mercenary_hire_cost_mult = 0.15
	}
	custom = more_expensive_court_positions
}

culture_era_late_medieval = { #Early Iron Age
	year = 1500
	invalid_for_government = tribal_government
	
	culture_modifier = {
		culture_tradition_max_add = 1
	}

	modifier = {
		mercenary_hire_cost_mult = 0.15
	}
	custom = more_expensive_court_positions
}
